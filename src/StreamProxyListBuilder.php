<?php

namespace Drupal\simple_proxy;

use Drupal\Core\Url;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBaseTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\simple_proxy\Plugin\StreamHandlerPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of stream proxies.
 */
class StreamProxyListBuilder extends DraggableListBuilder implements FormInterface {

  use ConfigFormBaseTrait;

  /**
   * The entities being listed.
   *
   * @var \Drupal\simple_proxy\Entity\StreamProxyInterface[]
   */
  protected $entities = [];

  /**
   * Stores the configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;


  /**
   * The stream handler plugin manager.
   *
   * @var \Drupal\simple_proxy\Plugin\StreamHandlerPluginManager
   */
  protected $streamHandlerManager;

  /**
   * Constructs a new StreamProxyListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\simple_proxy\Pluing\StreamHandlerManagerInterface $stream_handler_manager
   *   The stream handler plugin manager.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, ConfigFactory $config_factory, StreamHandlerPluginManager $stream_handler_manager) {
    parent::__construct($entity_type, $storage);
    $this->configFactory = $config_factory;
    $this->streamHandlerManager = $stream_handler_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('config.factory'),
      $container->get('plugin.manager.stream_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simple_proxy_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['simple_proxy.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = [
      'data' => $this->t('Label'),
    ];
    $header['plugin'] = [
      'data' => $this->t('Stream Handler'),
      'class' => [RESPONSIVE_PRIORITY_MEDIUM],
    ];
    $header['base_path'] = [
      'data' => $this->t('Base Path'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['file_path'] = [
      'data' => $this->t('File path'),
      'class' => [RESPONSIVE_PRIORITY_LOW],
    ];
    $header['status'] = [
      'data' => $this->t('Status'),
    ];

    $header = $header + parent::buildHeader();
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\simple_proxy\Entity\StreamProxyInterface $entity */
    $plugin = $entity->getPlugin();
    $row['label'] = $entity->label();
    $row['plugin']['#markup'] = $plugin->label();
    $row['base_path']['#markup'] = $plugin->getBasePath();
    $row['file_path']['#markup'] = $plugin->getConfigFilePath();
    $row['status']['#markup'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    $row = $row + parent::buildRow($entity);
    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $config = $this->configFactory->get('simple_proxy.settings');

    $form['origin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Origin website'),
      '#default_value' => $config->get('origin'),
      '#description' => $this->t("The origin website. For example: 'http://example.com'. If the site is using HTTP Basic Authentication (the browser popup for username and password) you can embed those in the url. Be sure to URL encode any special characters:<br/><br/>For example, setting a user name of 'myusername' and password as, 'letme&in' the configuration would be the following: <br/><br/>'http://myusername:letme%26in@example.com';"),
      '#required' => FALSE,
    ];

    $form['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      '#open' => FALSE,
    ];

    $form['settings']['verify'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Verify SSL'),
      '#default_value' => $config->get('verify'),
      '#description' => $this->t('Verifies the validity of the SSL certificate presented by the server when checked (if HTTPS is used).'),
      '#required' => FALSE,
    ];

    $form['settings']['hotlink'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hotlink'),
      '#default_value' => $config->get('hotlink'),
      '#description' => $this->t("When checked Stage File Proxy will not transfer the remote file to the local machine, it will just serve a 301 to the remote file and let the origin webserver handle it. This feature does not work with streams, in that case the files will be fetched."),
      '#required' => FALSE,
    ];

    $form['settings']['use_imagecache_root'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Image style Root'),
      '#default_value' => $config->get('use_imagecache_root') ?: TRUE,
      '#description' => $this->t("When checked, Stage File Proxy will look for /styles/ in the URL, determine the original file, and request that rather than the processed file. It will then send a header to the browser to refresh the image and let the image module create the derived image. This will speed up future requests for other derived images for the same original file."),
      '#required' => FALSE,
    ];

    $form['settings']['retry_failed_requests'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Retry failed requests'),
      '#default_value' => $config->get('retry_failed_requests'),
      '#description' => $this->t('If enabled, the proxy will retry requests that fail due to a timeout or other network error.'),
    ];

    $form['settings']['proxy_headers'] = [
      '#type' => 'textarea',
      '#title' => $this->t('HTTP headers'),
      '#default_value' => $config->get('proxy_headers'),
      '#description' => $this->t('When Stage File Proxy is configured to transfer the
        remote file to local machine, it will use this headers for HTTP request.
        Use format like "Referer|http://example.com/'),
      '#required' => FALSE,
    ];

    $form['settings']['credentials'] = [
      '#type' => 'link',
      '#title' => $this->t('Enter credentials'),
      '#url' => Url::fromRoute('simple_proxy.credentials_modal'),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'button',
        ],
      ],
    ];
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    $form['stream_proxy'] = [
      '#type' => 'details',
      '#title' => $this->t('Stream proxies'),
      '#open' => TRUE,
    ];

    $form['stream_proxy']['info'] = [
      '#type' => 'item',
      '#markup' => $this->t('Add a stream proxies to handle different paths and file types. The first matching stream proxy from the list will be used.'),
    ];

    $form['stream_proxy']['add_stream_proxy'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['container-inline'],
      ],
    ];
    // In order to prevent validation errors for the parent form, this cannot be
    // required, see self::validateAddStreamProxy().
    $form['stream_proxy']['add_stream_proxy']['stream_handler_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Stream handler'),
      '#empty_option' => $this->t('- Choose stream handler -'),
      '#options' => array_map(function ($definition) {
        return $definition['label'];
      }, $this->streamHandlerManager->getDefinitions()),
    ];
    $form['stream_proxy']['add_stream_proxy']['add_stream_proxy_submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add stream proxy'),
      '#validate' => ['::validateAddStreamProxy'],
      '#submit' => ['::submitAddStreamProxy'],
      '#limit_validation_errors' => [['stream_handler_type']],
    ];

    $form['stream_proxy'][$this->entitiesKey] = $form[$this->entitiesKey];
    $form['stream_proxy'][$this->entitiesKey]['#empty'] = $this->t('No stream proxies have been configured.');
    unset($form[$this->entitiesKey]);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Form validation handler for adding a new stream proxy.
   */
  public function validateAddStreamProxy(array &$form, FormStateInterface $form_state) {
    if ($form_state->isValueEmpty('stream_handler_type')) {
      $form_state->setErrorByName('stream_handler_type', $this->t('You must select the new stream handler type.'));
    }
  }

  /**
   * Form submission handler for adding a new stream proxy.
   */
  public function submitAddStreamProxy(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect(
      'entity.stream_proxy.add_form',
      ['stream_handler_type' => $form_state->getValue('stream_handler_type')]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Only save the entities if there are any.
    if ($form_state->getValue($this->entitiesKey)) {
      parent::submitForm($form, $form_state);
    }

    $keys = [
      'origin',
      'verify',
      'hotlink',
      'use_imagecache_root',
      'retry_failed_requests',
      'proxy_headers',
    ];

    $config = $this->configFactory->getEditable('simple_proxy.settings');
    foreach ($keys as $key) {
      $value = $form_state->getValue($key);
      if ($key === 'origin') {
        $value = trim($value, '/ ');
      }
      $config->set($key, $value);
    }
    $config->save();
  }

}

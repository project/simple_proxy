<?php

namespace Drupal\simple_proxy\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines stream_handler annotation object.
 *
 * @Annotation
 */
class StreamHandler extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The stream wrapper scheme.
   *
   * @var string
   */
  public $stream_wrapper_scheme;

}

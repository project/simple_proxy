<?php

namespace Drupal\simple_proxy;

use Drupal\simple_proxy\Entity\StreamProxy;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides an interface defining a Stream Proxy Manager.
 */
interface StreamProxyManagerInterface {

  /**
   * Find the first matching stream proxy from the weight sorted list.
   *
   * It will be filtere by base path, e.g. sites/default/files, and
   * then by the first matching file_path,
   * e.g. /sites/default/files/this/is/some/folder/file.jpg, where
   * 'this/is/some' would also match.
   */
  public function getStreamProxy(Request $request);

  /**
   * Fetches the file from the remote server.
   */
  public function fetch(StreamProxy $stream_proxy);

}

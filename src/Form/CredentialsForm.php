<?php

namespace Drupal\simple_proxy\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\TempStore\SharedTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Simple Proxy form.
 */
class CredentialsForm extends FormBase {


  /**
   * Shared storage.
   *
   * @var \Drupal\Core\TempStore\SharedTempStoreFactory
   */
  protected $sharedTempStore;

  /**
   * Constructs a new CredentialsForm object.
   *
   * @param \Drupal\Core\TempStore\SharedTempStoreFactory $shared_temp_store
   *   The tempstore service.
   */
  public function __construct(SharedTempStoreFactory $shared_temp_store) {
    $this->sharedTempStore = $shared_temp_store;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('tempstore.shared')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'simple_proxy_credentials';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $tempstore = $this->sharedTempStore->get('simple_proxy');
    $credentials = $tempstore->get('credentials');

    $form['#prefix'] = '<div id="modal_example_form">';
    $form['#suffix'] = '</div>';

    // The status messages that will contain any form errors.
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    $form['help'] = [
      '#type' => 'item',
      '#markup' => $this->t('Enter http auth credentials. They will be stored in the tempstore. Urlencoding will happen automatically.'),
    ];

    // A required checkboxes field.
    $form['use_http_auth'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use http auth credentials'),
      '#default_value' => $credentials['use_http_auth'] ?? '',
    ];

    if (!empty($credentials['user']) && !empty($credentials['password'])) {
      $form['text'] = [
        '#type' => 'item',
        '#markup' => $this->t('There are credentials saved in the tempstore.'),
      ];
    }

    // A required text field.
    $form['user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User'),
      // '#required' => TRUE,
    ];

    $form['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      // '#required' => TRUE,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save credentials'),
      '#attributes' => [
        'class' => [
          'use-ajax',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'submitModalFormAjax'],
        'event' => 'click',
      ],
    ];

    $form['actions']['delete'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete credentials'),
      '#attributes' => [
        'class' => [
          'use-ajax',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'deleteModalFormAjax'],
        'event' => 'click',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $form;
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   */
  public function submitModalFormAjax(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    // If there are any form errors, AJAX replace the form.
    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#modal_example_form', $form));
    }
    else {

      $tempstore = $this->sharedTempStore->get('simple_proxy');
      $tempstore->set('credentials', [
        'use_http_auth' => $form_state->getValue('use_http_auth'),
        'user' => $form_state->getValue('user'),
        'password' => $form_state->getValue('password'),
      ]);

      $response->addCommand(new OpenModalDialogCommand($this->t("Success!"), $this->t('The credentials got saved.'), ['width' => 700]));

    }

    return $response;
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   */
  public function deleteModalFormAjax(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    $tempstore = $this->sharedTempStore->get('simple_proxy');
    $tempstore->delete('credentials');

    $response->addCommand(new OpenModalDialogCommand($this->t("Success!"), $this->t('The credentials got deleted'), ['width' => 700]));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}

<?php

namespace Drupal\simple_proxy\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Stream Proxy form.
 *
 * @property \Drupal\simple_proxy\Entity\StreamProxyInterface $entity
 */
class StreamProxyAddForm extends StreamProxyFormBase {

  /**
   * The stream proxy entity.
   *
   * @var \Drupal\simple_proxy\Entity\StreamProxyInterface
   */
  protected $entity;

  /**
   * The stream handler plugin.
   *
   * @var \Drupal\simple_proxy\Plugin\StreamHandlerInterface
   */
  protected $plugin;

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return 'stream_proxy_entity_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $stream_handler_type = NULL) {
    $this->entity->setPlugin($stream_handler_type);
    $this->plugin = $this->entity->getPlugin();
    return parent::buildForm($form, $form_state);
  }

}

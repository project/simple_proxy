<?php

namespace Drupal\simple_proxy\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Stream Proxy form.
 *
 * @property \Drupal\simple_proxy\Entity\StreamProxyInterface $entity
 */
class StreamProxyEditForm extends StreamProxyFormBase {


}

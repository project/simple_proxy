<?php

namespace Drupal\simple_proxy\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface for stream_handler plugins.
 */
interface StreamHandlerInterface extends PluginInspectionInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Returns the translated plugin description.
   *
   * @return string
   *   The translated description.
   */
  public function description();

  /**
   * Sets the stream proxy ID.
   *
   * @param string $streamProxyId
   *   The stream proxy ID.
   */
  public function setstreamProxyId($streamProxyId);

}

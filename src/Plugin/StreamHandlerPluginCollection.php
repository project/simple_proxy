<?php

namespace Drupal\simple_proxy\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Plugin\DefaultSingleLazyPluginCollection;

/**
 * StreamHandler plugin manager.
 */
class StreamHandlerPluginCollection extends DefaultSingleLazyPluginCollection {
  /**
   * The unique ID for the search page using this plugin collection.
   *
   * @var string
   */
  protected $streamProxyId;

  /**
   * Constructs a new SearchPluginCollection.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $manager
   *   The manager to be used for instantiating plugins.
   * @param string $instance_id
   *   The ID of the plugin instance.
   * @param array $configuration
   *   An array of configuration.
   * @param string $stream_proxy_id
   *   The unique ID of the search page using this plugin.
   */
  public function __construct(PluginManagerInterface $manager, $instance_id, array $configuration, $stream_proxy_id) {
    parent::__construct($manager, $instance_id, $configuration);

    $this->streamProxyId = $stream_proxy_id;
  }

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\simple_proxy\StreamHandlerInterface
   *   The plugin instance.
   */
  public function &get($instance_id) {
    return parent::get($instance_id);
  }

  /**
   * {@inheritdoc}
   */
  protected function initializePlugin($instance_id) {
    parent::initializePlugin($instance_id);

    $plugin_instance = $this->pluginInstances[$instance_id];
    if ($plugin_instance instanceof StreamHandlerInterface) {
      $plugin_instance->setstreamProxyId($this->streamProxyId);
    }
  }

}

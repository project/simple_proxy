<?php

namespace Drupal\simple_proxy\Plugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;

/**
 * Provides a base implementation for a configurable StreamHandler plugin.
 */
abstract class ConfigurableStreamHandlerPlugin extends StreamHandlerPluginBase implements ConfigurableInterface, DependentPluginInterface, PluginFormInterface {

  use StringTranslationTrait;

  /**
   * The stream proxy ID.
   *
   * @var string
   */
  protected $streamProxyId;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ModuleHandlerInterface $module_handler,
    StreamWrapperManagerInterface $stream_wrapper_manager,
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $module_handler, $stream_wrapper_manager, $config_factory);

    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'file_path' => '',
      'exclude_file_extensions' => '',
      'base_path' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      '#open' => FALSE,
    ];

    $form['settings']['file_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('File path'),
      '#description' => $this->t('The file path the proxy will be responsible for. This is the path you set for a file/image field. Leave blank for all files.'),
      '#default_value' => $this->configuration['file_path'],
    ];
    $form['settings']['exclude_file_extensions'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Exclude file extensions'),
      '#description' => $this->t('Comma separated list of file extensions to exclude from proxying. Leave blank for no exclusions.'),
      '#default_value' => $this->configuration['exclude_file_extensions'],
    ];

    $form['settings']['base_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base path'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['file_path'] = trim($form_state->getValue('file_path'), '/');
    $this->configuration['exclude_file_extensions'] = $form_state->getValue('exclude_file_extensions');
    $this->configuration['base_path'] = trim($form_state->getValue('base_path'), '/');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigFilePath() {
    return $this->configuration['file_path'];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = NestedArray::mergeDeep($this->defaultConfiguration(), $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getstreamProxyId() {
    return $this->streamProxyId;
  }

  /**
   * {@inheritdoc}
   */
  public function setstreamProxyId($streamProxyId) {
    $this->streamProxyId = $streamProxyId;
  }

}

<?php

namespace Drupal\simple_proxy\Plugin\StreamHandler;

use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\simple_proxy\Plugin\ConfigurableStreamHandlerPlugin;

/**
 * Plugin implementation of the stream_handler.
 *
 * @StreamHandler(
 *   id = "public",
 *   stream_wrapper_scheme = "public",
 *   label = @Translation("Public"),
 *   description = @Translation("Public description.")
 * )
 */
class PublicStreamHandler extends ConfigurableStreamHandlerPlugin {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ModuleHandlerInterface $module_handler,
    StreamWrapperManagerInterface $stream_wrapper_manager,
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $module_handler, $stream_wrapper_manager, $config_factory);

    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
    $configuration,
    $plugin_id,
    $plugin_definition,
    $container->get('module_handler'),
    $container->get('stream_wrapper_manager'),
    $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form += parent::buildConfigurationForm($form, $form_state);

    // Set the default value for the base path.
    $form['settings']['base_path']['#default_value'] = $this->configuration['base_path'] ?: 'sites/default/files';
    $form['settings']['base_path']['#description'] = $this->t('The base path of the stream, e.g. sites/default/files.');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function parseFetchInfo(Request $request) {
    $request_path_info = rawurldecode($request->getPathInfo());

    $fetch_info = [];

    $fetch_info = parent::parseFetchInfo($request);
    if ($fetch_info === FALSE) {
      return FALSE;
    }

    $fetch_info['local_path'] = $this->streamWrapper()->getDirectoryPath();

    $this->moduleHandler->alter('simple_proxy_fetch_info', $fetch_info, $request);

    return $fetch_info;
  }

}

<?php

namespace Drupal\simple_proxy\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * StreamHandler plugin manager.
 */
class StreamHandlerPluginManager extends DefaultPluginManager {

  /**
   * Constructs StreamHandlerPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/StreamHandler',
      $namespaces,
      $module_handler,
      'Drupal\simple_proxy\Plugin\StreamHandlerInterface',
      'Drupal\simple_proxy\Annotation\StreamHandler'
    );
    $this->alterInfo('stream_handler_info');
    $this->setCacheBackend($cache_backend, 'stream_handler_plugins');
  }

}

<?php

namespace Drupal\simple_proxy\Plugin;

use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\UrlHelper;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;

/**
 * Base class for stream_handler plugins.
 */
abstract class StreamHandlerPluginBase extends PluginBase implements ContainerFactoryPluginInterface, StreamHandlerInterface {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperInterface
   */
  protected $streamWrapper;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The request info.
   *
   * This array contains information about the request
   * the plugin is about to handle.
   *
   * For a request like:
   * system/files/styles/large/private/path/to/folder/2023-05/image.jpg?itok=PLsa1pAa
   *
   * The array could contains the following keys:
   * - base_path: system/files
   * - style_path: styles/large/private
   * - file_path: path/to/folder
   * - file_name: image.jpg
   * - query: array with query parameters
   * - local_path: ../private_files
   *
   *  Alter hooks can be used to add more keys to this array.
   *  @ see hook_simple_proxy_fetch_info_alter()
   *  @ see hook_simple_proxy_remote_url_alter()
   *
   * @var array
   */
  protected $fetchInfo = [
    'base_path' => NULL,
    'style_path' => NULL,
    'style_name' => NULL,
    'file_path' => NULL,
    'file_name' => NULL,
    'query' => NULL,
    'local_path' => NULL,
  ];

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ModuleHandlerInterface $module_handler,
    StreamWrapperManagerInterface $stream_wrapper_manager,
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->moduleHandler = $module_handler;
    $this->streamWrapperManager = $stream_wrapper_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getFetchInfo(Request $request = NULL) {
    if (!$this->fetchInfo['file_name'] && $request) {
      if ($fetchInfo = $this->parseFetchInfo($request)) {
        $this->setFetchInfo($fetchInfo);
      }
    }
    return $this->fetchInfo;
  }

  /**
   * {@inheritdoc}
   */
  public function setFetchInfo(array $fetch_info) {
    $this->fetchInfo = $fetch_info;
  }

  /**
   * {@inheritdoc}
   */
  public function parseFetchInfo(Request $request) {
    $request_path = rawurldecode($request->getPathInfo());

    // Check if the request path starts with the base path.
    if (strpos($request_path, '/' . $this->configuration['base_path']) !== 0) {
      return FALSE;
    }

    // Check if file path is set and the request contains it.
    if (!empty($this->configuration['file_path']) && strpos($request_path, $this->configuration['file_path']) !== FALSE) {
      return FALSE;
    }

    if (!empty($this->configuration['exclude_file_extensions'])) {
      $exclude_file_extensions = array_map('trim', explode(',', $this->configuration['exclude_file_extensions']));
      $path_info = pathinfo($request_path);
      if (in_array($path_info['extension'], $exclude_file_extensions)) {
        return FALSE;
      }
    }

    $fetch_info = [];

    $fetch_info['base_path'] = $this->configuration['base_path'];
    $fetch_info['file_name'] = basename($request_path);
    $fetch_info['query'] = $this->parseQueryParameters($request);

    // For image styles, we rely on the image module.
    // @see PathProcessorImageStyles::processInbound()
    if (strpos($request_path, '/' . $this->configuration['base_path'] . '/styles/') === 0) {
      $path_prefix = '/' . $this->configuration['base_path'] . '/styles/';
      $path = substr($request_path, strpos($request_path, $path_prefix), strlen($request_path));

      // Strip out path prefix.
      $rest = preg_replace('|^' . preg_quote($path_prefix, '|') . '|', '', $path);

      // Get the image style, scheme and path.
      if (substr_count($rest, '/') >= 2) {
        [$image_style, $scheme, $file_path] = explode('/', $rest, 3);

        $style_path = 'styles/' . $image_style . '/' . $scheme;
        $fetch_info['style_path'] = (string) $style_path;
        $fetch_info['style_name'] = $image_style;
        $fetch_info['file_path'] = dirname($file_path);
      }
    }
    else {
      $path_prefix = '/' . $this->configuration['base_path'] . '/';
      $path = substr($request_path, strpos($request_path, $path_prefix), strlen($request_path));
      $rest = preg_replace('|^' . preg_quote($path_prefix, '|') . '|', '', $path);
      $fetch_info['file_path'] = dirname($rest);
    }

    // @todo: get the stream proxy id into the fetch info array.
    // $fetch_info['stream_proxy_id'] = $this->getstreamProxyId();

    return $fetch_info;
  }

  /**
   * {@inheritdoc}
   */
  public function parseQueryParameters(Request $request) {
    $query = $request->query->all();
    $query_parameters = UrlHelper::filterQueryParameters($query);
    unset($query_parameters['file']);
    return $query_parameters;
  }

  /**
   * Get values from the fetch info array.
   */
  private function getFromFetch(array $keys) {
    $fetch_info = $this->getFetchInfo();
    $values = [];
    foreach ($keys as $key) {
      if (isset($fetch_info[$key])) {
        $values[$key] = $fetch_info[$key];
      }
    }
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function fileExists() {
    $file_path = $this->getFromFetch([
      'local_path', 'style_path', 'file_path', 'file_name',
    ]);

    // @todo Is it necessary to alter filename here?
    $file_path = implode('/', $file_path);

    return file_exists($file_path);
  }

  /**
   * {@inheritdoc}
   */
  public function localDir() {
    $fetch_info = $this->getFetchInfo();
    $config = $this->configFactory->get('simple_proxy.settings');

    $local_dir = $this->getFromFetch([
      'local_path', 'style_path', 'file_path',
    ]);

    if ($local_dir['style_path'] && $config->get('use_imagecache_root')) {
      unset($local_dir['style_path']);
    }

    $this->moduleHandler->alter('simple_proxy_local_dir', $local_dir, $fetch_info);

    return implode('/', $local_dir);
  }

  /**
   * {@inheritdoc}
   */
  public function remoteUrl() : string {
    $fetch_info = $this->getFetchInfo();
    $config = $this->configFactory->get('simple_proxy.settings');

    $remote_url['origin'] = $config->get('origin');

    $remote_url += $this->getFromFetch([
      'base_path', 'style_path', 'file_path',
    ]);

    $remote_url['file_name'] = $this->fileName();

    // If the image style path is set and the imagecache root is not used.
    if (isset($remote_url['style_path']) && $config->get('use_imagecache_root')) {
      unset($remote_url['style_path']);
    }

    $this->moduleHandler->alter('simple_proxy_remote_url', $remote_url, $fetch_info);

    return implode('/', $remote_url);
  }

  /**
   * {@inheritdoc}
   */
  public function fileName() : string {
    $fetch_info = $this->getFetchInfo();
    $file_name = $this->getFromFetch(['file_name'])['file_name'];

    $this->moduleHandler->alter('simple_proxy_file_name', $file_name, $fetch_info);

    return $file_name;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $fetch_info = $this->getFetchInfo();

    $query = $this->getFromFetch([
      'query',
    ]);

    $this->moduleHandler->alter('simple_proxy_query', $query, $fetch_info);

    return $query;
  }

  /**
   * Get the corresponding stream wrapper.
   *
   * @return \Drupal\Core\StreamWrapper\StreamWrapperInterface
   *   The stream wrapper.
   */
  public function streamWrapper() : StreamWrapperInterface {
    if (!$this->streamWrapper) {
      $this->streamWrapper = $this->streamWrapperManager->getViaScheme($this->streamWrapperScheme());
    }
    return $this->streamWrapper;
  }

  /**
   * {@inheritdoc}
   */
  public function label() : string {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function description() : string {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function streamWrapperScheme() : string {
    return (string) $this->pluginDefinition['stream_wrapper_scheme'];
  }

  /**
   * Get the base path from the configuration.
   */
  public function getBasePath() : string {
    return $this->configuration['base_path'];
  }

}

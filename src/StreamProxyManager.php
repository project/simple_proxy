<?php

namespace Drupal\simple_proxy;

use Drupal\Core\Config\ConfigFactory;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;
use Drupal\Core\Utility\Error;
use GuzzleHttp\ClientInterface;
use Drupal\Core\File\FileSystemInterface;
use GuzzleHttp\Exception\GuzzleException;
use Drupal\Core\Lock\LockBackendInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Service description.
 */
class StreamProxyManager implements StreamProxyManagerInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * The lock.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected LockBackendInterface $lock;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected ConfigFactory $config;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  private Client $client;

  /**
   * Constructs a StreamProxyManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock.
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    FileSystemInterface $file_system,
    LoggerInterface $logger,
    LockBackendInterface $lock,
    ConfigFactory $config,
    ModuleHandlerInterface $module_handler,
    ClientInterface $client
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fileSystem = $file_system;
    $this->logger = $logger;
    $this->lock = $lock;
    $this->config = $config;
    $this->moduleHandler = $module_handler;
    $this->client = $client;
  }

  /**
   * Get the stream proxy for the current request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Drupal\simple_proxy\Entity\StreamProxy|null
   *   The stream proxy entity or null if none found.
   */
  public function getStreamProxy(Request $request) {
    // Iterate through all proxies and find the first one that matches.
    $stream_proxy = NULL;
    $stream_proxiy_storage = $this->entityTypeManager->getStorage('stream_proxy');
    $results = $stream_proxiy_storage->getQuery()
      ->condition('status', 1)
      ->sort('weight', 'ASC')
      ->execute();
    $stream_proxies = $stream_proxiy_storage->loadMultiple($results);

    foreach ($stream_proxies as $proxy) {
      /** @var \Drupal\simple_proxy\Entity\StreamProxy $proxy */
      $fetch_info = $proxy->getPlugin()->getFetchInfo($request);

      if (!is_null($fetch_info['file_name'])) {
        $stream_proxy = $proxy;
        break;
      }
    }

    $this->moduleHandler->alter('simple_proxy_stream_proxy', $stream_proxy, $request);

    return $stream_proxy;
  }

  /**
   * Fetch the file from the remote server.
   *
   * @param \Drupal\simple_proxy\Entity\StreamProxy $stream_proxy
   *   The stream proxy entity.
   *
   * @return bool
   *   True if the file was fetched successfully, false otherwise.
   */
  public function fetch($stream_proxy) {
    $url = $stream_proxy->getPlugin()->remoteUrl();

    $lock_id = 'stage_file_proxy:' . md5($url);
    $this->lock->release($lock_id);
    while (!$this->lock->acquire($lock_id)) {
      $this->lock->wait($lock_id, 1);
    }

    try {
      $config = $this->config->get('simple_proxy.settings');

      $options = [
        'verify' => $config->get('verify'),
        'Connection' => 'close',
      ];

      // Add headers if any.
      if (!empty($config->get('proxy_headers'))) {
        $options['headers'] = $this->createProxyHeadersArray($config->get('proxy_headers'));
      }

      if ($query = $stream_proxy->getPlugin()->query()) {
        $options['query'] = array_pop($query);
      }

      $response = $this->client->get($url, $options);
      // Check if the file was found.
      $result = $response->getStatusCode();

      if ($result != 200) {
        $this->logger->warning('HTTP error @errorcode occurred when trying to fetch @remote.', [
          '@errorcode' => $result,
          '@remote' => $url,
        ]);
        $this->lock->release($lock_id);
        return FALSE;
      }

      // Check if the download is complete.
      $content_length_headers = $response->getHeader('Content-Length');
      $content_length = array_shift($content_length_headers);
      $response_data = $response->getBody()->getContents();
      if (isset($content_length) && strlen($response_data) != $content_length) {
        $this->logger->error('Incomplete download. Was expecting @content-length bytes, actually got @data-length.', [
          '@content-length' => $content_length,
          '@data-length' => $content_length,
        ]);
        $this->lock->release($lock_id);
        return FALSE;
      }

      // Prepare local directory.
      $destination_dir = $stream_proxy->getPlugin()->localDir();

      if (!$this->fileSystem->prepareDirectory($destination_dir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
        $this->logger->error('Unable to prepare local directory @path.', ['@path' => $destination_dir]);
        $this->lock->release($lock_id);
        return FALSE;
      }

      // Save file.
      $destination_file = $destination_dir . '/' . $stream_proxy->getPlugin()->fileName();

      if ($this->writeFile($destination_file, $response_data)) {
        $this->lock->release($lock_id);
        return TRUE;
      }

      // If we get here, the file could not be saved.
      $this->logger->error('@remote could not be saved to @path.',
        [
          '@remote' => $url,
          '@path' => $destination_file,
        ]);
      $this->lock->release($lock_id);
      return FALSE;

    }
    catch (GuzzleException $e) {
      $this->logger->error(
        'Simple Proxy encountered an error when retrieving file @url. @message in %function (line %line of %file).',
        Error::decodeException($e) + ['@url' => $url]);
      $this->lock->release($lock_id);
      return FALSE;
    }

    return FALSE;
  }

  /**
   * Use write & rename instead of write.
   *
   * Perform the replace operation. Since there could be multiple processes
   * writing to the same file, the best option is to create a temporary file in
   * the same directory and then rename it to the destination. A temporary file
   * is needed if the directory is mounted on a separate machine; thus ensuring
   * the rename command stays local.
   *
   * @param string $destination
   *   A string containing the destination location.
   * @param string $data
   *   A string containing the contents of the file.
   *
   * @return bool
   *   True if write was successful. False if write or rename failed.
   */
  protected function writeFile($destination, $data) {
    // Get a temporary filename in the destination directory.
    $dir = $this->fileSystem->dirname($destination) . '/';
    $temporary_file = $this->fileSystem->tempnam($dir, 'simple_proxy_');
    $temporary_file_copy = $temporary_file;

    // Get the extension of the original filename and append it to the temp file
    // name. Preserves the mime type in different stream wrapper
    // implementations.
    $parts = pathinfo($destination);
    $extension = '.' . $parts['extension'];
    if ($extension === '.gz') {
      $parts = pathinfo($parts['filename']);
      $extension = '.' . $parts['extension'] . $extension;
    }
    // Move temp file into the destination dir if not in there.
    // Add the extension on as well.
    $temporary_file = str_replace(substr($temporary_file, 0, strpos($temporary_file, 'simple_proxy_')), $dir, $temporary_file) . $extension;

    // Preform the rename, adding the extension to the temp file.
    if (!@rename($temporary_file_copy, $temporary_file)) {
      // Remove if rename failed.
      @unlink($temporary_file_copy);
      return FALSE;
    }

    // Save to temporary filename in the destination directory.
    $filepath = $this->fileSystem->saveData($data, $temporary_file, FileSystemInterface::EXISTS_REPLACE);

    // Perform the rename operation if the write succeeded.
    if ($filepath) {
      if (!@rename($filepath, $destination)) {
        // Unlink and try again for windows. Rename on windows does not replace
        // the file if it already exists.
        @unlink($destination);
        if (!@rename($filepath, $destination)) {
          // Remove temporary_file if rename failed.
          @unlink($filepath);
        }
      }
    }

    // Final check; make sure file exists and is not empty.
    $result = FALSE;
    if (file_exists($destination) && filesize($destination) > 0) {
      $result = TRUE;
    }
    return $result;
  }

  /**
   * Helper function to generate HTTP headers array.
   *
   * @param string $headers_string
   *   The headers string.
   *
   * @return array
   *   The headers array.
   */
  protected function createProxyHeadersArray($headers_string) {
    $lines = explode("\n", $headers_string);
    $headers = [];
    foreach ($lines as $line) {
      $header = explode('|', $line);
      if (count($header) > 1) {
        $headers[$header[0]] = $header[1];
      }
    }
    return $headers;
  }

}

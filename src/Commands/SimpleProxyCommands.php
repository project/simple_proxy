<?php

namespace Drupal\simple_proxy\Commands;

use Drush\Commands\DrushCommands;
use Drupal\Core\Database\Connection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\Console\Helper\ProgressBar;

/**
 * Drush commands for Simple Proxy.
 */
class SimpleProxyCommands extends DrushCommands {

  /**
   * The database service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;


  /**
   * The app root.
   *
   * @var string
   */
  protected $root;

  /**
   * StageFileProxyCommands constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param string $root
   *   The app root.
   */
  public function __construct(
    Connection $database,
    ConfigFactoryInterface $config_factory,
    string $root
  ) {
    parent::__construct();
    $this->database = $database;
    $this->configFactory = $config_factory;
    $this->root = $root;
  }

  /**
   * Command description here.
   *
   * @param string $arg1
   *   Argument description.
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   *
   * @command simpleproxy:download
   * @aliases spd
   *
   * @option stream_proxy_id
   *   The stream proxy ID.
   * @option fid
   *  The file ID.
   */
  public function download($arg1 = 'test', $options = [
    'stream_proxy_id' => '',
    'fid' => 0,
  ]) {

    $logger = $this->logger();
    $server = $this->configFactory->get('simple_proxy.settings')->get('origin');
    if (empty($server)) {
      throw new \Exception('Configure simple_proxy.settings.origin in your settings.php (see INSTALL.txt).');
    };

    $query = $this->database->select('file_managed', 'fm')
      ->fields('fm', ['uri'])
      ->orderBy('fm.fid', 'DESC');

    $fid = $options['fid'];
    if ($fid > 0) {
      $query->condition('fm.fid', $fid);
    }

    $results = $query->execute()
      ->fetchCol();

    $count_results = count($results);

    $logger->notice('Downloading {count} files.', [
      'count' => $count_results,
    ]);

    $downloaded_files_count = 0;

    $progress_bar = new ProgressBar($this->output(), $count_results);
    foreach ($results as $uri) {
      $progress_bar->advance();

      // @todo download file
      $downloaded_files_count++;
    }

    $progress_bar->finish();
    $this->output()->writeln('');

    $logger->notice('Downloaded {downloaded_files_count} files.', [
      'downloaded_files_count' => $downloaded_files_count,
    ]);

  }

}

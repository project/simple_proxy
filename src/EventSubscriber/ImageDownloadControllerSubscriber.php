<?php

namespace Drupal\simple_proxy\EventSubscriber;

use Drupal\Core\Routing\RoutingEvents;
use Drupal\Core\Routing\RouteBuildEvent;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\simple_proxy\Controller\ImageStyleDownloadController;

/**
 * Decorates core's image download controller with our own.
 */
class ImageDownloadControllerSubscriber implements EventSubscriberInterface {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Overwrite the _controller key to point to our controller.
   *
   * @param \Drupal\Core\Routing\RouteBuildEvent $event
   *   The event containing the route being built.
   */
  public function onAlterDecorateController(RouteBuildEvent $event): void {
    $config = $this->configFactory->get('simple_proxy.settings');
    if (!$config->get('retry_failed_requests')) {
      return;
    }
    $to_alter = [
      'image.style_public',
      'image.style_private',
    ];
    foreach ($to_alter as $name) {
      $definition = $event->getRouteCollection()->get($name);
      if ($definition) {
        $definition->setDefault('_controller', ImageStyleDownloadController::class . "::deliver");
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[RoutingEvents::ALTER] = 'onAlterDecorateController';
    return $events;
  }

}

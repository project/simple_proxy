<?php

namespace Drupal\simple_proxy\EventSubscriber;

use Drupal\Core\Url;
use Psr\Log\LoggerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\simple_proxy\StreamProxyManagerInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Simple Proxy event subscriber.
 */
class SimpleProxySubscriber implements EventSubscriberInterface {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The stream proxy manager.
   *
   * @var \Drupal\simple_proxy\StreamProxyManagerInterface
   */
  protected StreamProxyManagerInterface $streamProxyManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\simple_proxy\StreamProxyManagerInterface $stream_proxy_manager
   *   The stream proxy manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StreamProxyManagerInterface $stream_proxy_manager, LoggerInterface $logger) {
    $this->configFactory = $config_factory;
    $this->streamProxyManager = $stream_proxy_manager;
    $this->logger = $logger;
  }

  /**
   * Kernel request event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   Response event.
   */
  public function onKernelRequest(RequestEvent $event) : void {

    $request = $event->getRequest();

    // Get the configuration.
    $config = $this->configFactory->get('simple_proxy.settings');
    // Quit if no origin given. Means the module is not configured yet.
    if (!$config->get('origin')) {
      return;
    }

    // Get the stream proxy, if any for this request.
    if (!$stream_proxy = $this->streamProxyManager->getStreamProxy($request)) {
      return;
    }

    // Quit if the file already exists.
    if ($stream_proxy->getPlugin()->fileExists()) {
      return;
    }

    $location = NULL;

    if ($config->get('hotlink')) {
      // Note that for hotlink we want to use the path to the originally
      // requested file and not the one from which it was generated.
      $location = Url::fromUri($stream_proxy->getPlugin()->remoteUrl(), [
        'query' => $stream_proxy->getPlugin()->getQuery(),
        'absolute' => TRUE,
      ])->toString();
    }
    elseif ($this->streamProxyManager->fetch($stream_proxy)) {

      // Refresh this request & let the web server work out mime type, etc.
      $location = Url::fromUri('base://' . ltrim(rawurldecode($request->getPathInfo()), '/'), [
        'query' => $stream_proxy->getPlugin()->query(),
        'absolute' => TRUE,
      ])->toString();
      // Avoid redirection caching in upstream proxies.
      header("Cache-Control: must-revalidate, no-cache, post-check=0, pre-check=0, private");
    }
    else {
      $this->logger->error('Failed to fetch file @file', ['@file' => $request->getPathInfo()]);
    }

    return;
    if ($location) {
      header("Location: $location");
      exit;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => ['onKernelRequest'],
    ];
  }

}

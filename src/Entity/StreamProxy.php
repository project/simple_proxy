<?php

namespace Drupal\simple_proxy\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\simple_proxy\Plugin\StreamHandlerPluginCollection;

/**
 * Defines the stream proxy entity type.
 *
 * @ConfigEntityType(
 *   id = "stream_proxy",
 *   label = @Translation("Stream Proxy"),
 *   label_collection = @Translation("Stream Proxies"),
 *   label_singular = @Translation("stream proxy"),
 *   label_plural = @Translation("stream proxies"),
 *   label_count = @PluralTranslation(
 *     singular = "@count stream proxy",
 *     plural = "@count stream proxies",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\simple_proxy\StreamProxyListBuilder",
 *     "form" = {
 *       "add" = "Drupal\simple_proxy\Form\StreamProxyAddForm",
 *       "edit" = "Drupal\simple_proxy\Form\StreamProxyEditForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "stream_proxy",
 *   admin_permission = "administer stream_proxy",
 *   links = {
 *     "collection" = "/admin/config/development/stream-proxy",
 *     "edit-form" = "/admin/config/development/stream-proxy/{stream_proxy}",
 *     "delete-form" = "/admin/config/development/stream-proxy/{stream_proxy}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "weight" = "weight",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "weight",
 *     "plugin",
 *     "configuration",
 *   }
 * )
 */
class StreamProxy extends ConfigEntityBase implements StreamProxyInterface, EntityWithPluginCollectionInterface {

  /**
   * The stream proxy ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The stream proxy label.
   *
   * @var string
   */
  protected $label;

  /**
   * The stream proxy status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The weight of the stream proxy.
   *
   * @var int
   */
  protected $weight;

  /**
   * The configuration of the stream handler plugin.
   *
   * @var array
   */
  protected $configuration = [];

  /**
   * The stream handler plugin id.
   *
   * @var string
   */
  protected $plugin;

  /**
   * The stream handler plugin collection.
   *
   * @var \Drupal\simple_proxy\Plugin\StreamHandlerPluginCollection
   */
  protected $pluginCollection;

  /**
   * Encapsulates the creation of the search page's LazyPluginCollection.
   *
   * @return \Drupal\Component\Plugin\LazyPluginCollection
   *   The search page's plugin collection.
   */
  protected function getPluginCollection() {
    if (!$this->pluginCollection) {
      $this->pluginCollection = new StreamHandlerPluginCollection($this->streamHandlerPluginManager(), $this->plugin, $this->configuration, $this->id());
    }
    return $this->pluginCollection;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginCollections() {
    return ['configuration' => $this->getPluginCollection()];
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin() {
    return $this->getPluginCollection()->get($this->plugin);
  }

  /**
   * {@inheritdoc}
   */
  public function setPlugin($plugin_id) {
    $this->plugin = $plugin_id;
    $this->getPluginCollection()->addInstanceID($plugin_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function postCreate(EntityStorageInterface $storage) {
    parent::postCreate($storage);
    if (!isset($this->weight)) {
      $this->weight = 0;
    }
  }

  /**
   * Wrapper method for the stream handler plugin manager.
   */
  public function streamHandlerPluginManager() {
    return \Drupal::service('plugin.manager.stream_handler');
  }

}

<?php

namespace Drupal\simple_proxy\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a stream proxy entity type.
 */
interface StreamProxyInterface extends ConfigEntityInterface {

  /**
   * Returns the stream handler plugin.
   *
   * @return object
   *   The stream handler plugin.
   */
  public function getPlugin();

  /**
   * Sets the stream handler plugin.
   *
   * @param string $plugin_id
   *   The stream handler plugin.
   */
  public function setPlugin($plugin_id);

  /**
   * Returns the stream handler plugin configuration.
   *
   * @return array
   *   The stream handler plugin configuration.
   */
  public function getConfiguration();

}

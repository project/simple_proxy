# Simple Proxy

A simple Proxy to download files from a remote server.

## Description

This is an approach to have a highly configurable an extensible file proxy.
This project evolved from stage_file_proxy with the need to be able to
handle more stream wrappers and hanlde folders differently. It with build
with the idea in mind that it could be customized to all kinds of different needs,
because drupal setups can also  be highly customized.

The basic idea is to have stream_proxy config entities with a configurable plugin attached
to handle incoming file requests. The config entities are sortable in a list and a mechanism
greps the first that fits to the incoming request by comparing a configurable path.
That way different folders can be handled differently. If you have two stream proxies

/sites/default/files/path/to/folder
/sites/default/files/path/

path/to/folder can be handled differently then all other folders in /path.

The logic is encapsulated in stream_handler plugins that are tight to a stream wrapper
with the configuration value stream_wrapper_scheme. So it is possible to write custom plugins
and use them instead of the two coming with this module or have modules that provide stream
wrappers bring their own plugins.

## Configuration

- Goto /admin/config/development/stream-proxy and set the origin of the remote server.
- Choose a stream handler from the dropdown, e.g. Public and add a stream proxy.
- Give it a label and enable it in to form.
- With all other fields left it will handle all files for a stream wrapper.
- Add more stream stream proxies with more fingrained settings.
- Order the list to have the most specific on top. Keep in mind that the first on that
  has a fitting path will be picked. See StreamProxyManager::getStreamProxy()

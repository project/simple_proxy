<?php

/**
 * @file
 * Hooks related to Simple Proxy module.
 */

use Symfony\Component\HttpFoundation\Request;

// phpcs:disable DrupalPractice.CodeAnalysis.VariableAnalysis.UnusedVariable

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the proxy used for a certain request.
 *
 * @param array $proxy
 *   The proxy array.
 * @param \Symfony\Component\HttpFoundation\Request $request
 *   The request object.
 *
 * @see \Drupal\simple_proxy\StreamProxyManager::getStreamProxy()
 */
function hook_simple_proxy_stream_proxy_alter(array &$proxy, Request $request) {

}

/**
 * Alter the fetch info array.
 *
 * @param array $fetch_info
 *   The fetch info array.
 * @param \Symfony\Component\HttpFoundation\Request $request
 *   The request object.
 *
 * @see \Drupal\simple_proxy\Plugin\StreamPluginBase::parseFetchInfo()
 */
function hook_simple_proxy_fetch_info_alter(array &$fetch_info, Request $request) {

}

/**
 * Alter the file name.
 *
 * @param string $file_name
 *   The file name.
 * @param array $fetch_info
 *   The fetch info array.
 *
 * @see \Drupal\simple_proxy\Plugin\StreamPluginBase::fileName()
 */
function hook_simple_proxy_file_name_alter(string &$file_name, array $fetch_info) {

}

/**
 * Alter the remote URL.
 *
 * @param string $remote_url
 *   The remote URL origin as set in settings form.
 * @param array $fetch_info
 *   The fetch info array.
 */
function hook_simple_proxy_remote_url_alter(string &$remote_url, array $fetch_info) {
  $config = \Drupal::config('my_module.settings');
  $port = $config->get('port');
  if (!empty($port)) {
    $remote_url['origin'] .= $port;
  }
}

/**
 * @} End of "addtogroup hooks".
 */
